"""
pyregex

re.match looks for a match *from the beginning* of string
re.search looks for pattern *ANYWHERE* in string

re.match( r'(.*) are (.*?) .*', line, re.M|re.I)
re.I = case insensitive matching
re.M
re.search(pattern, string, flags=0)

re{ n}
match exactly n number of expressions

Identifiers
'(ab)*'
\d any number
\D anything but a num
\w any char
\W anything but a char
. any char except newline
\b whitespace around words

Quantifier: amount
{1,3} : looking from 1-3 chars
+ match 1 or more
? match 0 or 1
* match 0 or more
^ match beg of string
$ match end of str
| either or
[] range
{x} expecting x amount

Combined Identifier and Quantifier
\d{3} = give me three digits in a row


re.match(r'[^b]at')

Task: Rip out
Goal is end up with This, 2 columns.

minute,number_of_messages
Dec  3 00:02,1
Dec  3 00:03,6

1st regex - ^(.*? \d+ \d+\:\d+).*
2nd regexp - ^(.*? \d+ \d+\:\d+)\:\d+ .*? (.*?)\: .*
matches


* is a greedy quantifier, which means that it will match as many letters possible.
*? is a non greedy quantifier, it will match the smallest number of letters possible

"""

import re

# Get at our file, first open, then read it
# file = open('fake.log')
# logline = file.readlines()
"""
1) make function to do the regex parse
2) with open sttmnt to open, then for each line get time, add time to dict counter
3) print a sorted list of time, count

with statement: automatically closes file.
with open('filename.txt') as file: # file is file object
you can then step through the file using a
for line in f: loop
"""
def findme(line):
    # rip out times first
    result = re.match(r"^(.*\d\d\:\d+)", line).group(1)
    return result


timehash ={}
with open('fake.log') as f: # use pythonic struture to open and read line
    for line in f:
        time = findme(line)
        if time not in timehash:
            timehash[time] = 1 # we found a new one!
        else:
            timehash[time] += 1 # INCREMNETs value for that specific

# print sorted based on keys
for time,count in sorted(timehash.items()):
    print(time,count)

"""
# print it sorting by COUNTS

for time,count in sorted(timehash.items(),key=lambda x:x[1]):
    print(time,count)
"""


# Extract the minutes
#re.match(regexp, line).groups()
# match.group(2) for example gives you the second group blob

# would match any 3 letter except 'bat'
