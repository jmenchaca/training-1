# LPTHW ex13.py

# pull in argv from sys library/module
from sys import argv
# Here we unpack the command line arguments and assign them to variables
script, first, second, third = argv

print("The script is called:", script)
print("The first name is", first)
print("The second name is", second)
print("your third name is:", third)
