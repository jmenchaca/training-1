# Operators

# Both comparisona and logical products
# return true or false
#
# comparison
# <, <=
# >, >=
# !=
#
# logical
#
# and (a and b)
# or  (a or b)
# not not(a and b)

totalcars = 4 + 4

answer = 2 + 3 * 4
print(answer)

# Use a logical operator in between 2 comparison ones
print((5 > 3) and (4 < 7))
