# classes.py

"""You can use this class to represent how classy someone
or something is.
"Classy" is interchangable with "fancy".
If you add fancy-looking items, you will increase
your "classiness".
Create a function in "Classy" that takes a string as
input and adds it to the "items" list.

Another method should calculate the "classiness"
value based on the items.
The following items have classiness points associated
with them:
"tophat" = 2
"bowtie" = 4
"monocle" = 5
Everything else has 0 points.
Use the test cases below to guide you!"""

# Define the classy object and
# init funciton is called automatically when an instantion of class
# is created

class Classy(object):
    def __init__(self): # Called when a new self objects are created
        self.items = []

# Create the additem function to append
    def addItem(self, item):
        self.items.append(item)

# define getclassiness fn to caluclate getClassiness
# Modify the self.item list to add items as they come in.
    def getClassiness(self):
        if len(self.items > 0): # check make sure we have items in list
            for item in self.items:
                classiness = 0
                if item == "tophat":
                    classiness += 2
                elif item == "bowtie":
                    classiness += 4
                elif item == "monocle":
                    classiness +=5
        return classiness

# create new classy object
me = Classy()

# Should be 0
print(me.getClassiness())

me.addItem("tophat")
# Should be 2
print me.getClassiness()

me.addItem("bowtie")
me.addItem("jacket")
me.addItem("monocle")
# Should be 11
print(me.getClassiness())

me.addItem("bowtie")
# Should be 15
print(me.getClassiness())
