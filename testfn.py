def testfn(bar):
    return bar*2 # Takes in our input and * 2

print(testfn)
print(type(testfn))
print(testfn(2) > 3)

print("Lets give testfn a value of 5, returns {}".format(testfn(5)))
