"""
Python Requests
https://realpython.com/python-requests/#getting-started-with-requests
Guy who wrote the lib
http://docs.python-requests.org/en/master/user/quickstart/

"""
import requests


# Do an HTTP get method on a URL
response = requests.get('https://www.github.com/fruit')
#print(response)
#Login to Github to get acct access
# response = requests.get('https://api.github.com'), auth=('user','pass'))
# response.status_code


# if response.status_code == 200:
#     print('Success!!!')
#
# elif response.status_code == 404:
#     print('not found')

# Can use boolean also directly.
# Evalates as True if response between 2 & 400. false otherwise.
if response:
    print('response success! We got a {}'.format(response.status_code))
else:
    print('Fail: Response code: {}'.format(response.status_code))

# pull the raw html content, spits aback in fugly way
# print(response.text)
# print headers, returns the requests headers in a dict format
# print(response.headers)
print(response.headers['Date'])

# Query string params in the get()
print(requests.get(
    'https://api.github.com/search/repositories',
    params={'q': 'requests+language:python'},
))

# use HTTP/requests post to create new dict item
# Sent to httpbin to get test response
print(requests.post('https://httpbin.org/post', data={'animal':'monkey'}))
