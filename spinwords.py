"""
# https://www.codewars.com/kata/stop-gninnips-my-sdrow/train/python
# Stop spinning my words!

Write a function that takes in a string of one or more words, and returns the same string,
but with all five or more letter words reversed (Just like the name of this Kata).
Strings passed in will consist of only letters and spaces. Spaces will be included
only when more than one word is present.


Examples:

spinWords( "Hey fellow warriors" ) => returns "Hey wollef sroirraw"
spinWords( "This is a test") => returns "This is a test"
spinWords( "This is another test" )=> returns "This is rehtona test"
"""

def spin_words(sentence):
    # split splits it into an array for me, how convenient
    spinstr = ''
    for word in sentence.split(' '):
        if len(word) < 5 and len(sentence) == 1:
            return word# if there's less then 5 characters, just return

        # if its a short word and more than one word in sentence need a ' '
        elif len(word) < 5 and len(sentence) > 1:
            spinstr = word + ' '
        # otherwise add in reverse of word and a space
        else:
            spinstr += word[::-1] + ' '
    return spinstr

s = 'Welcome'
sentence = 'hefel hefel on a pefel'
print(spin_words(s))
print(spin_words('This is a test'))


"""

def spin_words(sentence):
    output = []
    for word in sentence.split(' '):
        if len(word) > 4:
            word = word[::-1]
        output.append(word)
    return ' '.join(output)

def spin_words(sentence):
    # Your code goes here
    return " ".join([x[::-1] if len(x) >= 5 else x for x in sentence.split(" ")])


def spin_words(sentence):
    words = [word for word in sentence.split(" ")]
    words = [word if len(word) < 5 else word[::-1] for word in words]
    return " ".join(words)
