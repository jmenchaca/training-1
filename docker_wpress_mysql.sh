#######
#!/usr/bin/env bash

# Wordpress + MySQL project

# Make the volume
# Create & run mysql db
#create * run wpress container

#Create base structure for both the containers
# https://docs.docker.com/storage/volumes/

# create bridge network, even though theres one by default in docker
docker network create bridgenet

# create the mysql db, peristsent vol, and connect to network
docker run --name db -e MYSQL_ROOT_PASSWORD=boringdefault -d mysql:latest \
-v dbvolume \
--network skynet \

# create wpress container & tie to network
docker run --name wpress --network some-network -d wordpress -p 8000:80 \
--network skynet \
-e WORDPRESS_DB_HOST=wpress \
-e WORDPRESS_DB_USER=admin \
-e WORDPRESS_DB_PASSWORD=boringdefault \
-e WORDPRESS_DB_NAME=db \
/
#-e WORDPRESS_TABLE_PREFIX=...



########
