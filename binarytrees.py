"""
Binary Trees

# binary tree
A tree whose elements have at most 2 children is a binary tree

Tree consists of
1) data
2) pointer to left child
3) pointer to rt child


"""

# Inorder
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


"""
class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        result = []
        if root:
            result = self.inorderTraversal(root.left)
            result.append(root.val)
            result = result + self.inorderTraversal(root.right)
        return result

"""

#

class Node:
    def __init__(self, root, children):
        self.root = root
        self.children = children


tree = Node("Flynn Mackie - Senior VP of Engineering",
            [Node("Wesley Thomas - VP of Design",
                  [Node("Randall Cosmo - Director of Design",
                        [Node("Brenda Plager - Senior Designer", [])])]), Node("Nina Chiswick - VP of Engineering",
                                                                               [Node(
                                                                                   "Tommy Quinn - Director of Engineering",
                                                                                   [Node(
                                                                                       "Jake Farmer - Frontend Manager",
                                                                                       [Node(
                                                                                           "Liam Freeman - Junior Code Monkey",
                                                                                           [])]), Node(
                                                                                       "Sheila Dunbar - Backend Manager",
                                                                                       [Node(
                                                                                           "Peter Young - Senior Code Cowboy",
                                                                                           [])])])])])


# define Function to printperson root, print through children
# call printperson recursively inside

def printperson(node, tab):  # tree is whole tree, root including
    print('\t' * tab + node.root)
    tab += 1

    for child in node.children:
        printperson(child, tab) # recurse deeper into tree by calling printperson with child as node


# call the printperson function to traverse through the tree
printperson(tree, 0) # tab should be zero when we start
