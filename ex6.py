# LPTHW - Strings and Text
# Introducing f-Strings

types_of_people = 10
x = f"There are {types_of_people} types of people."

binary = "binary"
do_not = "don't"
# Here wer'e using the f string format

# assign an f string to y, (includes embedded vars)
y = f"Those who know {binary} and those who {do_not}"

# print values of x and y
print(x)
print(y)

# print the f string
print(f"I said: {x}")
print("I also said: '{y}'")

hilarious = False
joke_evaluation = "Isn't that joke so funny?! {}"

print(joke_evaluation.format(hilarious))

w = "This is the left side of..."
e = "a string with a right side."

print(w + e)


######################################
hot_girl = "Diana"
t = f"don'tcha wanna know that {hot_girl}"
