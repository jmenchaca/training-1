"""
looping
-------------------------------------------------------------
For loops: loop through a list/array
-------------------------------------------------------------
for num in range(1, 5):
    print counter


-------------------------------------------------------------
While loops: runs until a condition fails, until condition
-------------------------------------------------------------

while True: #
    reply = raw_input("Enter text [type stop to quit]:")
    print(reply.lower())
    if reply == "stop":
        break


#Make a for loop that prints out dictionary stuff
#items returns list of dictionaries key:value pairs
# dict.items format
# Putting sep separator equal to empty string makes it delete the spaces between

# for name, price in menu_prices.items():
#     print(name, ':', price, sep='')

"""

names = ["Joey Tribbiani", "Monica Geller", "Chandler Bing", "Phoebe Buffay"]
#
# usernames = []
# for name in names:
#     usernames.append(name.replace(' ','_').lower())
#
# print(usernames)


for name in names:
    name = name.lower().replace(" ", "_")
    print(name)
print(names)

# Notice how we loop through this number to find multiples, but
# if we do range (n) it doesnt include up to n itself, its n - 1
print("\nlets loop through a range of numbers")
for i in range(5):
    print(i)


# if we want to include 5 as the range max, we have to use n + 1 as max range
n = 5
print("\nNow a for loop that includes up to 'n' itself, n={}".format(n))
for i in range(n+1):
    print(i)

names = ['apple', 'banana', 'pear', 'peach']

for name in names:
    print(name)
