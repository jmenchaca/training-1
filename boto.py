"""
BOTO3 practice
examples: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/ec2-examples.html


Hit the ground running w/ BOTO
1) pip install boto3 #first install the library w pip
2) Setup credentials w/ 'aws configure', ~/aws/credentials


EXAMPLES: s3 bkt and ec2 waiter

s3 = boto3.resource('s3')
bucket = s3.Bucket('my-bucket')
for bucket in s3.buckets.all():
    print(bucket.name, bucket.last_modified)


ec2 = boto3.client('ec2')

waiter = ec2.get_waiter('instance_running')
waiter.wait(InstanceIds=['i-abc123'])

print('Instance is ready!')

BOTO Classes:
Client: for low-level EC2 activities, create vpc, gateway etc.
client = boto3.client('ec2')

Instance: Act on an instance, when you know ID already
ec2 = boto3.resource('ec2') # create the ec object, boto3 resource
instance = ec2.Instance('id')

"""
import boto3

"""
Client filter parameters

instance-type
ip-address
host-id
"""
#describe all instances

ec2 = boto3.client('ec2')

print("First we'll print out the filtered info: \n")
response = ec2.describe_instances(
Filters=[
    {
        'Name': 'instance-type',
        'Values': [
            'string',
        ]
    },
],
MaxResults=123
)

print(response)

# print("\nNext we're printing out a massive ugly blob\n")
# response = ec2.describe_instances()
# print(response)


#################################################################
# Create instance
print("\nHere we go making an instance: ")
ec2 = boto3.resource('ec2')
instances = ec2.create_instances(
    ImageID='ami-0349dbb9f7baf92f9',
    MinCount=1,
    MaxCount=2,
    InstanceType='t2.micro',
    KeyName='mbpro'
)
