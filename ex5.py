# LPTHW ex 5: More variables and printing

name = 'Ethan Clansky'
age = 35
height = round(76 * 2.54) # in to cm. 1 in = 2.54cm
weight = round(190 / 2.2) # 2.2 kg in 1 lb
eyes = 'Brown'
teeth = 'White'
hair = 'Brown'
food = 'Chicken'

# Print stuff using a string formatter
print(f"Let's make a cool string with {food} inside\n")

print(f"Let's talk about {name}")
print(f"He's {height} cm tall.")
print(f"He's {weight} kg heavy.")
print("Actually that's not too heavy.")
print(f"He's got {eyes} eyes and {hair} hair.")
print(f"His teeth are usually {teeth} depending on how much tea he had recently")

# this line is tricky, try to get it exactly right
total = age + height + weight
print(f"If I add {age}, {height}, and {weight} I get {total}.")
