"""
merge sort

1) create function that takes in array
2) find midpoint of that array
3) assign the halves using string splicing
4) create merge fn to

"""
def merge_sort(array):

    if len(array) <= 1:
        return array

    midpoint = int(len(array) / 2)
    print("The mmidpoint is {}".format(midpoint))

    # define left and right. left = the function
    left, right = merge_sort(array[:midpoint]), merge_sort(array[midpoint:])

    return merge(left, right)

# fn to merge left & right arrays
def merge(left, right):

    result = []
    lft_indx = rt_indx = 0

    # while theyre's still stuff in left and right array
    while lft_indx < len(left) and rt_indx < len(right):
        if left[lft_indx] < right[rt_indx]:

            result.append(left[lft_indx])
            lft_indx += 1

        else:

            result.append(right[rt_indx])
            rt_indx += 1

    # If its not dropping into the while loop above, add the rest of left or rt array onto result
    result.extend(left[lft_indx:])
    result.extend(right[rt_indx:])
    print("result is now {}".format(result))

    return result

def main():
    array = [5, 4, 3, 1, 14, 7, 3, 1]
    print(array)

    result = merge_sort(array)
    print(result)

if __name__ == "__main__":
    main()
