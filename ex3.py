# LPTHW

# Comparison operators give boolean result of True or False
# <, >, <=, >=
# The rest are normal math operators
# note using print("xyz", ) then comma, then other stuff prints
# everything on the same line, rather than dumping to new line

print("I will now count my chickens:")

print("Hens", 25 + 30 / 6)
print("Roosters", 100-25 * 3 % 4)

print("Now I will count the eggs:")

print(3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6)

print("Is it true that 3 + 2 < 5 - 7?")

print(3 + 2 < 5 - 7)

print("what is 3 + 2?", 3 + 2)
print("What is 5-7?", 5 - 7)

print("Oh, that's why it's False.")

print("How about some more.")

print("Is it greater?", 5 > -2)
print("Is it great or equal?", 5 >= -2)
print("Is it less or equal?", 5 <= -2)
