""" codewars
https://www.codewars.com/kata/515de9ae9dcfc28eb6000001/train/python

1 create result []
2 use for loop step of 2 up to len(s) to step through string
3
4
"""

# def solution(s):
#     result = []
#     for i in range(0, len(s), 2): # (start, stop, step)
#         if len(s)/2 == 0: # if string is even lenth, do normal split
#             result.append(s[i:i+2])
#             print(result)
#         elif not(len(s)/2 == 0): # if string is odd length, add the '_'
#             s += '_'
#             result.append(s[i:i+2])
#             print(result)
#
#     return result


################################################################
# Another solution
################################################################

# Kata answer, why does len(s) % 2 true and
def solution(s):
    result = []
    if len(s) % 2: # Odd number test. if len(s)%1 = 1 = True,
        s += '_'
    for i in range(0, len(s)):
        result.append(s[i:i+2])
        print(result)
    return result

print('Results list: {}'.format(solution('asdfadsff')))

# this is an odd number conditional test...wtf!?
# Boolean: True = 1, False = 0
# % odds % 2 = 1, even = 0
if 9 % 2:
    print('Condition is true')
else:
    print('Condition is false')
