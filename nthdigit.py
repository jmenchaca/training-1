# Codewars: find nth digit of a number
# 9/25/2018
"""
Complete the function that takes two numbers as input, num and nth and return the nth digit of num (counting from right to left).

Note
If num is negative, ignore its sign and treat it as a positive value
If nth is not positive, return -1
Keep in mind that 42 = 00042. This means that findDigit(42, 5) would return 0
"""

def find_digit(num, nth):
    # first make the num a string
    num = str(num)
    # 1st test if nth <= 0,
    # if it is we return -1
    if nth <= 0:
        return -1
    # elif condition when len(num) < nth, just return 0
    elif len(num) < nth:
        return 0
    # else for all 'normal' cases do this
    else:
        # assign num the inverted index of nth, and return it as an int
        num = num[-nth]
        return int(num)



find_digit(5673, 4) # 5
find_digit(129, 2) # 2
print(find_digit(-2825, 3)) # 8
print(find_digit(319582, 4)) # 9

"""
findDigit(5673, 4)     returns 5
findDigit(129, 2)      returns 2
findDigit(-2825, 3)    returns 8
findDigit(-456, 4)     returns 0
findDigit(0, 20)       returns 0
findDigit(65, 0)       returns -1
findDigit(24, -8)      returns -1
"""



"""
def find_digit(num, nth):
    num = str(num)
    if nth<=0: return -1
    elif nth> len(num): return 0
    else:
        num = num[::-1]
        return int(num[nth-1])


def find_digit(num, n):
    num = str(abs(num))
    return -1 if n < 1 else 0 if n > len(num) else int(num[-n])


def find_digit(num, nth):
    if nth<=0:
        return -1
    if num<0:
        num = -num
    return int((num/10 ** (nth-1))%10)
"""
