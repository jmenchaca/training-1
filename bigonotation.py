"""
Big O Notation

Nested loop creates a O(n^2) situation
O(n) going through array linearly

for a in list:
    print a

Runtime of square plot of grass
O(a)


1) O(a+b)
 Different steps get added
    do step 1) O(a)
       step 2) O(b)

2) Drop constants: we care about is it linear or quadratic (n^2) relationship
   take a function
    for each e in array
        min = MIN(e, min)
    for each e in array:
        max = MAX(e, new)

3) diff inputs use diff variables
   This is not O(N^2), its O(a x b) cuz
   its a diff array

    for a in arrayA{
        for b in arrayB{
            if a==b{
                count +=1
            }
        }
    }

4)

            }
        }
    }
"""
