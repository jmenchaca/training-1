# ex15.py
# reading files
# read. returns whole file as stirng, including newlines etc
 #readline: returns line by line including newline 

from sys import argv

# assign cmd line arguments to
script, filename = argv
# open filename and store it as a FILE OBJECT
txt = open(filename)

print(f"Here's your file {filename}:")
# running read on an opened file stores whole file as one string newlines included
print(txt.read())

print("Type the filename again:")
file_again = input("> ")

# open the txt_agai variable
txt_again = open(file_again)
print("txt_again is of type: {}\n".format(type(txt_again)))

# Print the opened and read filename, in this case its actually the input
print(txt_again.read())
