# regex practice from 0
"""
1) create fn to do the parsing, use re.match
2) create with sttmnt to loop through the lines, calls findme(), and loads hash
3) make for loop to print out time hash

"""
import re

# regex fn acts on indiv line
def findme(line):
    m = re.match(r"(.*?\d\d\:\d+)", line)#.group(1)
    if m:
        m = re.match(r"(.*?\d\d\:\d+)", line).group(1)
        return m
    else:
        print('No match object found!')

# with sttmnt opens file
with open('fake.log') as f:
    timehash = {}
    for line in f: # loop through
        time = findme(line) # result of fn is the time
        if time not in timehash: # if time entry !, create it
            timehash[time] = 1
        else:
            timehash[time] +=1


#print(timehash)

for time,count in timehash.items():
    print(time, count)
