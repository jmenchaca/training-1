# LPTHW ex11: Asking Questions, Inputs etc

# end usually spits out new line. by assigning it to
# end = ' '  we eliminate the newline

print("How old are you?", end =' ')
age = input()
print("How tall are you?", end =' ')
height = input()
print("how much do you weight?", end=' ')
weight = input()

print(f"So, you're, {age} old, {height} tall and {weight} heavy.")

##################################################
print("How many languages do you speak?", end = ' ')
langnum = input()
print("Where did you learn the first one?", end = ' ')
firstloc = input()
print("Which do you remember the best now?", end = ' ')
bestmem = input()

# lets print a formatted string
print(f"So you speak {langnum} languages, you learned the first one at {firstloc}, and you remember {bestmem} the best")
