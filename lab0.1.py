# lab 0.1: Python

import sys
# Lab 0.1: Processing Input & Output
#  5 XP, +5 XP for adding line count
# In this program, you create a program that acts like the command cat in Linux or Unix, or type in Windows.
#
# Do the following:
#
# read from standard input and process each line, saving it to a variable, e.g. current_line
# print the variable current_line with each iteration
# (bonus) pre-pend each line with a line count, followed by a colon, :.
# You can run the program like this in Line:
#
# cat passwd | ./lab0_1
# ./lab0_1 < passwd

# To open a py file to use it, first open it then, read it
#file = open('/home/eclan/repos/training/etcpasswd','r')
# test_file.read()
# testfile.close()
#
#
# Take in the file as standard in on cmd line <
file = sys.stdin

# native python construct to loop through a file line by line.
for line in file:
    current_line = line # Assign each line to current_line
    print(current_line, end='')
# can also use file.readline() to pull in each sequential line of the file
# file = ./etcpasswd
# for line in file: # Use a for loop to go through file line by line
#     print current_line # Print out current_line

# readline reads a SINGLE line in with the \n character at the end
#file.readline()

file.close()
