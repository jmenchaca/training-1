# Leetcode two sumDigits
"""
1) create enumerated dictionary that we'll add answer idx, value into
2) create diff varaible, i looping through whole range
3) append
"""
arr = [4,5,6,7,8,9,10]
target = 17
sums = []

def twoSum(arr, target):
    sums = {}
    for idx, val in enumerate(arr):
        diff = target - val # take differenc in target and current index value
        if diff in sums: # if that diff number is a key in sums dict, return
            print("sums diff: ", sums[diff])
            return [sums[diff], idx]
        sums[val] = idx # set key position (normally index) to the idx itself
        print('sums key value: ', sums['idx'])
        #print('sums arr: ', sums)
        # Assign the key(index), to idx so that when loop restarts
        # the index

print(twoSum(arr, target))


"""
collected = {}
for idx,val in enumerate(nums):
    diff = target - val
    if diff in collected:
        return [collected[diff], idx]
    collected[val] = idx


table = {}
for i,n in enumerate(nums):
    if target-n in table:
        return [i, table[target-n]]
    table[n] = i




=================================================
For finding a pair of numbers next to each other that sum to target
=================================================
def twosum(arr, target):
    for i in range(0,len(arr)):
        for j in range(i+1, len(arr)):
            if arr[i] + arr[j] == target:
                 sums.append(arr[i], arr[j])
    return sums



print(twosum(arr, target))

for i in range(0,len(arr)):
    print(i)
"""
